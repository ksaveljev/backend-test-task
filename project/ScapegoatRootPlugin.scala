import com.sksamuel.scapegoat.sbt.ScapegoatSbtPlugin.autoImport._
import sbt.{AutoPlugin, Def, _}
import sbt.Keys._
import ReportPlugin.autoImport._

/** The plugin is intended to be used on the root project. */
object ScapegoatRootPlugin extends AutoPlugin {

  override def requires: ReportPlugin.type = ReportPlugin

  private val output = Def.setting {
    file(scapegoatOutputPath.value) -> s"${thisProject.value.id}-${scalaBinaryVersion.value}"
  }

  override def projectSettings: Seq[Def.Setting[_]] = inConfig(Scapegoat) {
    ReportPlugin.makeSettings(Scapegoat) ++ Seq(
      reportSources := output.all(ReportPlugin.aggregates).value,
      reportTarget := (target in Scapegoat).value / "scapegoat-report",
      reportIndex := "scapegoat.html"
    )
  }
}
