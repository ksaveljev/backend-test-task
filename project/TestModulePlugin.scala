import sbt._
import sbt.Keys._
import sbt.plugins.JUnitXmlReportPlugin

/** The plugin is intended to be used on modules the root project aggregates.
  *
  *  The modules are intended to have the unit tests only. All the integration
  *  and functional tests belong to the root project.
  */
class TestModulePlugin extends AutoPlugin {
  import TestModulePlugin._
  import autoImport._

  def defaultSettings: Seq[Setting[_]] =
    disableDefaultExporter ++
      Seq(
        scalaTestVersion := "3.0.5",
        pegdownVersion := "1.6.0"
      )

  // we only require JUnitXmlReportPlugin here to load _after_ it, to disable it
  override def requires: Plugins = JUnitXmlReportPlugin

  // we disable exporter here, because we use `-u` parameter to ScalaTest
  // and this parameter makes it generate the same reports
  protected val disableDefaultExporter: Seq[Setting[_]] =
  testListeners :=
    testListeners.value filterNot (_.isInstanceOf[JUnitXmlTestsListener])

  protected def addLibraries(config: Configuration): Seq[Setting[_]] =
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % scalaTestVersion.value % config,
      "org.pegdown" % "pegdown" % pegdownVersion.value % config
    )

  protected def makeSettings(config: Configuration): Seq[Setting[_]] =
    Seq(
      testOptions := Seq(
        Tests.Argument(
          TestFrameworks.ScalaTest,
          "-oDS",
          "-u", (target.value / s"${config.name}-reports").toString,
          "-h", (target.value / s"${config.name}-reports").toString
        )
      ),
      fork := true,
      testForkedParallel := true,
      logBuffered := false
    )

}

object TestModulePlugin extends TestModulePlugin {

  object autoImport {
    lazy val scalaTestVersion = settingKey[String]("ScalaTest version to use")
    lazy val pegdownVersion = settingKey[String]("Pegdown version to use")
  }

  override def projectSettings: Seq[sbt.Setting[_]] =
    defaultSettings ++
      addLibraries(Test) ++
      inConfig(Test)(makeSettings(Test))
}
