// required to generate the beautiful aggregation page
libraryDependencies ++= Seq(
  "org.webjars.npm" % "bootstrap" % "4.0.0",
  "org.webjars.npm" % "jquery"    % "3.3.1"
)

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")
addSbtPlugin("com.sksamuel.scapegoat" %% "sbt-scapegoat" % "1.0.9")
