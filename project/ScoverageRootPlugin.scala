import ReportPlugin.autoImport._
import sbt.{AutoPlugin, Def, _}
import sbt.Keys._
import scoverage.ScoverageSbtPlugin.ScoveragePluginConfig

/** The plugin is intended to be used on the root project. */
object ScoverageRootPlugin extends AutoPlugin {

  override def requires: ReportPlugin.type = ReportPlugin

  override def projectSettings: Seq[Def.Setting[_]] = inConfig(ScoveragePluginConfig) {
    ReportPlugin.makeSettings(ScoveragePluginConfig) ++ Seq(
      reportSources := Seq(
        crossTarget.value / "scoverage-report" ->
          s"scala-${scalaBinaryVersion.value}"
      ),
      reportTarget := (target in ScoveragePluginConfig).value / "scoverage-report"
    )
  }
}
