import sbt._

object Dependencies {
  private object versionOf {
    // internal

    // external
    val akka = "2.5.19"
    val akkaPersistenceCassandra = "0.93"
    val enumeratum = "1.5.13"
    val logback = "1.2.3"
    val scalaLogging = "3.9.2"

    // test
    val scalaTest = "3.0.5"
  }

  object internalDependencies {
  }

  object externalDependencies {
    val akkaActor                = "com.typesafe.akka"          %% "akka-actor"                 % versionOf.akka                     withSources()
    val akkaSlf4j                = "com.typesafe.akka"          %% "akka-slf4j"                 % versionOf.akka                     withSources()
    val akkaPersistence          = "com.typesafe.akka"          %% "akka-persistence"           % versionOf.akka                     withSources()
    val akkaPersistenceCassandra = "com.typesafe.akka"          %% "akka-persistence-cassandra" % versionOf.akkaPersistenceCassandra withSources()
    val enumeratum               = "com.beachape"               %% "enumeratum"                 % versionOf.enumeratum               withSources()
    val logback                  = "ch.qos.logback"              % "logback-classic"            % versionOf.logback                  withSources()
    val scalaLogging             = "com.typesafe.scala-logging" %% "scala-logging"              % versionOf.scalaLogging             withSources()
  }

  object testDependencies {
    val scalaTest                        = "org.scalatest"     %% "scalatest"                           % versionOf.scalaTest                % Test withSources()
    val akkaPersistenceCassandraLauncher = "com.typesafe.akka" %% "akka-persistence-cassandra-launcher" % versionOf.akkaPersistenceCassandra % Test withSources()
  }
}
