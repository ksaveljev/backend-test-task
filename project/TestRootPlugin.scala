import sbt._

/** The plugin is intended to be used on the root project.
  *
  *  The modules are intended to have the unit tests only. All the integration
  *  and functional tests belong to the root project.
  */
object TestRootPlugin extends TestModulePlugin {

  object autoImport {
    lazy val FunctionalTest = config("ft") extend Test
  }
  import autoImport._

  override def requires = super.requires && ReportPlugin

  override def projectConfigurations =
    Seq(IntegrationTest, FunctionalTest)

  override def projectSettings: Seq[sbt.Setting[_]] =
    defaultSettings ++
      addLibraries(IntegrationTest) ++
      addLibraries(FunctionalTest) ++
      inConfig(IntegrationTest)(Defaults.testSettings ++ makeSettings(IntegrationTest)) ++
      inConfig(FunctionalTest)(Defaults.testSettings ++ makeSettings(FunctionalTest)) ++
      inConfig(Test)(ReportPlugin.makeSettings(Test))
}
