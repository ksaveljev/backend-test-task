import sbt._
import sbt.Keys._
import sbt.AutoPlugin

object ReportPlugin extends AutoPlugin {

  object autoImport {

    lazy val reportSources = settingKey[Seq[(File, String)]]("Directories to take the reports from")
    lazy val reportTarget = settingKey[File]("Directory to aggregate reports to")
    lazy val reportIndex = settingKey[String]("Index file of aggregated reports")

    lazy val copyReports = taskKey[Seq[String]]("Copies reports produced by the tests")
    lazy val aggregateReports = taskKey[Unit]("Creates index file with reports found")

  }

  import autoImport._

  val aggregates: ScopeFilter =
    ScopeFilter(inAggregates(ThisProject, includeRoot = false))

  def makeSettings(config: Configuration): Seq[Setting[_]] =
    Seq(
      reportSources := {
        val projects = thisProject.all(aggregates).value
        projects map { project =>
          project.base / "target" / s"$config-reports" -> project.id
        }
      },
      reportTarget := (target in config).value / s"$config-reports",
      reportIndex := "index.html",
      copyReports := {
        val targetDirectory = (reportTarget in config).value
        val log = streams.value.log

        log.info(s"Copying reports into $target...")

        for {
          (directory, id) <- reportSources.value
          if directory.isDirectory
          targetReport = targetDirectory / id
        } yield {
          log.info(s"Copying report from $directory...")
          IO.copyDirectory(directory, targetReport)
          id
        }
      },
      aggregateReports := {
        val targetDirectory = (reportTarget in config).value
        val log = streams.value.log

        log.info(s"Generating aggregated report...")

        val projects = copyReports.value.sorted

        val tabs = projects.foldLeft("") { case (body, project) =>
          val firstTab = body.isEmpty
          val cssClass = if (firstTab) "nav-link active" else "nav-link"
          body +
            s"""
          <li class="nav-item">
            <a
              class="$cssClass"
              id="$project-tab"
              data-toggle="tab"
              href="#$project"
              role="tab"
              aria-controls="$project"
              aria-selected="$firstTab">
              $project
            </a>
          </li>
          """
        }

        val index = reportIndex.value
        val frames = projects.foldLeft("") { case (body, project) =>
          val firstTab = body.isEmpty
          val cssClass =
            if (firstTab) "tab-pane fade show active" else "tab-pane fade"
          body +
            s"""
          <div
            class="$cssClass"
            id="$project"
            role="tabpanel"
            aria-labelledby="$project-tab">
            <iframe
              width="100%"
              height="100%"
              frameborder="0"
              src="$project/$index">
            </iframe>
          </div>
          """
        }

        val contents =
          s"""
          <html>
            <head>
              <link rel="stylesheet" href="bootstrap.min.css">
              <script src="jquery.min.js"></script>
              <script src="bootstrap.min.js"></script>
            </head>
            <body>
              <ul class="nav nav-tabs" role="tablist">
                $tabs
              </ul>
              <div class="tab-content">
                $frames
              </div>
            </body>
          </html>
          """

        writeWebJar(targetDirectory, "jquery/3.3.1/dist", "jquery.min.js")
        writeWebJar(targetDirectory, "bootstrap/4.0.0/dist/css", "bootstrap.min.css")
        writeWebJar(targetDirectory, "bootstrap/4.0.0/dist/js", "bootstrap.min.js")
        IO.write(targetDirectory / "index.html", contents)
      }
    )

  private def writeWebJar(target: File, jar: String, file: String): Unit = {
    val contents = IO.readStream(
      getClass.getResourceAsStream(s"META-INF/resources/webjars/$jar/$file")
    )
    IO.write(target / file, contents)
  }
}
