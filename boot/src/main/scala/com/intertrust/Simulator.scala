package com.intertrust

import akka.actor.{ActorSystem, Props}
import com.intertrust.actors.{AlertsActor, EventConsumer, EventEmitter}
import com.intertrust.parsers.{MovementEventParser, TurbineEventParser}

import java.time.Instant
import scala.io.Source

object Simulator extends App {
  val system = ActorSystem("simulator")
  val speedUp = 60 // IMPROVE: pass it via param or from config
  val simulationStartingPoint = Instant.parse("2015-11-23T00:00:00.00Z")

  val alertsActor = system.actorOf(Props(classOf[AlertsActor]), "alerts")

  val movementEvents = new MovementEventParser().parseEvents(Source.fromResource("movements.csv"))
  val turbineEvents = new TurbineEventParser().parseEvents(Source.fromResource("turbines.csv"))

  val eventConsumer = system.actorOf(EventConsumer.props(speedUp, alertsActor))

  val movementEventsEmitter = system.actorOf(
    EventEmitter.props(
      name                    = "movementEvent",
      source                  = movementEvents,
      simulationStartingPoint = simulationStartingPoint,
      speedUp                 = speedUp,
      eventConsumer           = eventConsumer
    )
  )

  val turbineEventsEmitter = system.actorOf(
    EventEmitter.props(
      name                    = "turbineEvent",
      source                  = turbineEvents,
      simulationStartingPoint = simulationStartingPoint,
      speedUp                 = speedUp,
      eventConsumer           = eventConsumer
    )
  )
}
