package com.intertrust.state

import com.intertrust.state.EngineerState._
import com.intertrust.state.EventConsumerEvent._
import com.intertrust.state.EventConsumerCommand._
import com.intertrust.state.TurbineState._
import com.intertrust.protocol.Alert._
import com.intertrust.protocol.Location._
import com.intertrust.protocol.Movement._
import com.intertrust.protocol.TurbineStatus._
import org.scalatest.{Matchers, WordSpec}

import java.time.Instant
import scala.concurrent.duration._

class EventConsumerStateSpec extends WordSpec with Matchers {
  "EventConsumerState" should {
    "correctly identify if engineer has entered the turbine since particular timestamp" when {
      "there is no entry in state about specified turbineId" in new Scope {
        emptyState.hasEngineerEntered("testTurbineId", Instant.now) shouldBe false
      }

      "there is at least one engineer on the turbine" in new Scope {
        val turbineState = TurbineState(Broken, Instant.now, HasEngineers("testEngineerId", List.empty, Instant.now))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerEntered("testTurbineId", Instant.now) shouldBe true
      }

      "there is no engineer for shorter duration than the period we ask for" in new Scope {
        val turbineState = TurbineState(Broken, turbineBrokenSince, NoEngineer(afterTurbineBroke))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerEntered("testTurbineId", turbineBrokenSince) shouldBe true
      }

      "there is no engineer for longer duration that the period we ask for" in new Scope {
        val turbineState = TurbineState(Broken, turbineBrokenSince, NoEngineer(beforeTurbineBroke))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerEntered("testTurbineId", turbineBrokenSince) shouldBe false
      }

      "there is no engineer for the same duration that the period we ask for" in new Scope {
        val turbineState = TurbineState(Broken, turbineBrokenSince, NoEngineer(turbineBrokenSince))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerEntered("testTurbineId", turbineBrokenSince) shouldBe false
      }
    }

    "correctly identify if engineer has fixed the turbine after leaving it" when {
      "there is no entry in state about specified turbineId" in new Scope {
        emptyState.hasEngineerFixed("testTurbineId", Instant.now) shouldBe false
      }
      "turbine is working fine" in new Scope {
        val turbineState = TurbineState(Working, Instant.now, NoEngineer(Instant.now))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerFixed("testTurbineId", Instant.now) shouldBe true
      }
      "turbine is in broken state for shorter duration than we ask for" in new Scope {
        val turbineState = TurbineState(Broken, afterTurbineBroke, NoEngineer(Instant.now))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerFixed("testTurbineId", turbineBrokenSince) shouldBe true
      }
      "turbine is in broken state for longer duration than we ask for" in new Scope {
        val turbineState = TurbineState(Broken, turbineBrokenSince, NoEngineer(Instant.now))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerFixed("testTurbineId", afterTurbineBroke) shouldBe false
      }
      "turbine is in broken state for the same duration as we ask for" in new Scope {
        val turbineState = TurbineState(Broken, turbineBrokenSince, NoEngineer(Instant.now))
        val state = emptyState.copy(turbines = Map("testTurbineId" -> turbineState))
        state.hasEngineerFixed("testTurbineId", turbineBrokenSince) shouldBe false
      }
    }

    "correctly handle turbine status update while producing alerts and scheduling commands" when {
      "there is no entry in state about specified turbineId and turbine is Working" in new Scope {
        val event = TurbineStatusUpdate("testTurbineId", Working, eventTimestamp)
        val (updatedState, alert, commands) = emptyState.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(event.timestamp)))
        updatedState.engineers shouldBe Map.empty
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified turbineId and turbine is Broken" in new Scope {
        val event = TurbineStatusUpdate("testTurbineId", Broken, eventTimestamp)
        val (updatedState, alert, commands) = emptyState.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Broken, eventTimestamp, NoEngineer(event.timestamp)))
        updatedState.engineers shouldBe Map.empty
        alert shouldBe Some(TurbineAlert(eventTimestamp, "testTurbineId", "Turbine stopped working"))
        commands shouldBe List(ScheduleCommand(CheckIfStillBrokenAndNoEngineer("testTurbineId", eventTimestamp), 4.hours))
      }
      "turbine was Broken and its status didn't change" in new Scope {
        val turbines = Map("testTurbineId" -> TurbineState(Broken, turbineBrokenSince, NoEngineer(turbineBrokenSince)))
        val state = EventConsumerState(turbineBrokenSince, Map.empty, turbines)
        val event = TurbineStatusUpdate("testTurbineId", Broken, eventTimestamp)
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.engineers shouldBe Map.empty
        updatedState.turbines shouldBe state.turbines
        alert shouldBe None
        commands shouldBe List.empty
      }
      "turbine was Working and its status didn't change" in new Scope {
        val turbines = Map("testTurbineId" -> TurbineState(Working, afterTurbineBroke, NoEngineer(afterTurbineBroke)))
        val state = EventConsumerState(afterTurbineBroke, Map.empty, turbines)
        val event = TurbineStatusUpdate("testTurbineId", Working, eventTimestamp)
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.engineers shouldBe Map.empty
        updatedState.turbines shouldBe state.turbines
        alert shouldBe None
        commands shouldBe List.empty
      }
      "turbine went from Broken to Working" in new Scope {
        val turbines = Map("testTurbineId" -> TurbineState(Broken, turbineBrokenSince, NoEngineer(turbineBrokenSince)))
        val state = EventConsumerState(turbineBrokenSince, Map.empty, turbines)
        val event = TurbineStatusUpdate("testTurbineId", Working, eventTimestamp)
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.engineers shouldBe Map.empty
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(turbineBrokenSince)))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "turbine went from Working to Broken" in new Scope {
        val turbines = Map("testTurbineId" -> TurbineState(Working, afterTurbineBroke, NoEngineer(afterTurbineBroke)))
        val state = EventConsumerState(afterTurbineBroke, Map.empty, turbines)
        val event = TurbineStatusUpdate("testTurbineId", Broken, eventTimestamp)
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.engineers shouldBe Map.empty
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Broken, eventTimestamp, NoEngineer(afterTurbineBroke)))
        alert shouldBe Some(TurbineAlert(eventTimestamp, "testTurbineId", "Turbine stopped working"))
        commands shouldBe List(ScheduleCommand(CheckIfStillBrokenAndNoEngineer("testTurbineId", eventTimestamp), 4.hours))
      }
    }

    "correctly handle engineer movement update while producing alerts and scheduling commands" when {
      "there is no entry in state about specified engineerId and he enters vessel" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Enter, eventTimestamp)
        val (updatedState, alert, commands) = emptyState.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> OnShip("testShipId", event.timestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified engineerId and he enters turbine (turbineId not in state)" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val (updatedState, alert, commands) = emptyState.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, HasEngineers("testEngineerId", List.empty, eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified engineerId and he enters Working turbine" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(turbines = Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, NoEngineer(beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("testEngineerId", List.empty, eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified engineerId and he enters Broken turbine" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(turbines = Map("testTurbineId" -> TurbineState(Broken, beforeTurbineBroke, NoEngineer(beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Broken, beforeTurbineBroke, HasEngineers("testEngineerId", List.empty, eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified engineerId and he exits vessel" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Exit, eventTimestamp)
        val (updatedState, alert, commands) = emptyState.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(event.timestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified engineerId and he exits turbine (turbineId not in state)" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val (updatedState, alert, commands) = emptyState.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified engineerId and he exits Working turbine" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(turbines = Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, NoEngineer(beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "there is no entry in state about specified engineerId and he exits Broken turbine" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(turbines = Map("testTurbineId" -> TurbineState(Broken, beforeTurbineBroke, NoEngineer(beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Broken, beforeTurbineBroke, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe None
        commands shouldBe List(ScheduleCommand(CheckIfStillBrokenAfterEngineer("testTurbineId", eventTimestamp), 3.minutes))
      }

      "engineer was on ship1 and entered ship1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnShip("testShipId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> OnShip("testShipId", beforeTurbineBroke))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer entered ship [id=testShipId] without leaving ship [id=testShipId]"))
        commands shouldBe List.empty
      }
      "engineer was on ship1 and entered ship2" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("otherShipId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnShip("testShipId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> OnShip("otherShipId", eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer entered ship [id=otherShipId] without leaving ship [id=testShipId]"))
        commands shouldBe List.empty
      }
      "engineer was on ship1 and entered turbine1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnShip("testShipId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, HasEngineers("testEngineerId", List.empty, eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer entered turbine [id=testTurbineId] without leaving ship [id=testShipId]"))
        commands shouldBe List.empty
      }
      "engineer was on ship1 and left ship1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnShip("testShipId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer was on ship1 and left ship2" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("otherShipId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnShip("testShipId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer left ship [id=otherShipId] without leaving ship [id=testShipId]"))
        commands shouldBe List.empty
      }
      "engineer was on ship1 and left turbine1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnShip("testShipId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer left turbine [id=testTurbineId] without leaving ship [id=testShipId]"))
        commands shouldBe List.empty
      }
      "engineer was on turbine1 and entered ship1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnShip("testShipId", eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer entered ship [id=testShipId] without leaving turbine [id=testTurbineId]"))
        commands shouldBe List.empty
      }
      "engineer was on turbine1 and entered turbine1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer entered turbine [id=testTurbineId] without leaving turbine [id=testTurbineId]"))
        commands shouldBe List.empty
      }
      "engineer was on turbine1 and entered turbine2" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("otherTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)), "otherTurbineId" -> TurbineState(Working, eventTimestamp, HasEngineers("testEngineerId", List.empty, eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("otherTurbineId", eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer entered turbine [id=otherTurbineId] without leaving turbine [id=testTurbineId]"))
        commands shouldBe List.empty
      }
      "engineer was on turbine1 and left ship1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer left ship [id=testShipId] without leaving turbine [id=testTurbineId]"))
        commands shouldBe List.empty
      }
      "engineer was on turbine1 and left turbine1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer was on turbine1 and left turbine2" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("otherTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)), "otherTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer left turbine [id=otherTurbineId] without leaving turbine [id=testTurbineId]"))
        commands shouldBe List.empty
      }
      "engineer was outside and left ship1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> Outside(beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer left ship [id=testShipId] without entering"))
        commands shouldBe List.empty
      }
      "engineer was outside and left turbine1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> Outside(beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe Some(MovementAlert(eventTimestamp, "testEngineerId", "Engineer left turbine [id=testTurbineId] without entering"))
        commands shouldBe List.empty
      }
      "engineer was outside and entered ship1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Vessel("testShipId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> Outside(beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map.empty
        updatedState.engineers shouldBe Map("testEngineerId" -> OnShip("testShipId", eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer was outside and entered turbine1" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> Outside(beforeTurbineBroke)))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, eventTimestamp, HasEngineers("testEngineerId", List.empty, eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer enters turbine which already has an engineer in it" in new Scope {
        val event = EngineerUpdate("otherEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)), turbines = Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("testEngineerId", List.empty, beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("testEngineerId", List("otherEngineerId"), beforeTurbineBroke)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke), "otherEngineerId" -> OnTurbine("testTurbineId", eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer enters turbine which he is already in" in new Scope {
        val event = EngineerUpdate("otherEngineerId", Turbine("testTurbineId"), Enter, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)), turbines = Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("testEngineerId", List("otherEngineerId"), beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("testEngineerId", List("otherEngineerId"), beforeTurbineBroke)))
        updatedState.engineers shouldBe Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke), "otherEngineerId" -> OnTurbine("testTurbineId", eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer leaves turbine when he is the only engineer there" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)), turbines = Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("testEngineerId", List.empty, beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, NoEngineer(eventTimestamp)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer leaves turbine when he is not the only engineer there but was the first to enter" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke), "otherEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)), turbines = Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("testEngineerId", List("otherEngineerId"), beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("otherEngineerId", List.empty, beforeTurbineBroke)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp), "otherEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke))
        alert shouldBe None
        commands shouldBe List.empty
      }
      "engineer leaves turbine when he is not the only engineer there and was not the first to enter" in new Scope {
        val event = EngineerUpdate("testEngineerId", Turbine("testTurbineId"), Exit, eventTimestamp)
        val state = emptyState.copy(engineers = Map("testEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke), "otherEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke)), turbines = Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("otherEngineerId", List("testEngineerId"), beforeTurbineBroke))))
        val (updatedState, alert, commands) = state.update(event)
        updatedState.lastUpdate shouldBe eventTimestamp
        updatedState.turbines shouldBe Map("testTurbineId" -> TurbineState(Working, beforeTurbineBroke, HasEngineers("otherEngineerId", List.empty, beforeTurbineBroke)))
        updatedState.engineers shouldBe Map("testEngineerId" -> Outside(eventTimestamp), "otherEngineerId" -> OnTurbine("testTurbineId", beforeTurbineBroke))
        alert shouldBe None
        commands shouldBe List.empty
      }
    }
  }

  abstract class Scope {
    val emptyState = EventConsumerState(Instant.now, Map.empty, Map.empty)
    val eventTimestamp = Instant.parse("2019-02-03T10:25:30.00Z")
    val turbineBrokenSince = Instant.parse("2019-02-03T10:15:30.00Z")
    val beforeTurbineBroke = Instant.parse("2019-02-03T09:15:30.00Z")
    val afterTurbineBroke = Instant.parse("2019-02-03T11:15:30.00Z")
  }
}
