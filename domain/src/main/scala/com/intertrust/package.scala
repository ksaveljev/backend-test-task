package com

import java.time.Duration
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.FiniteDuration

package object intertrust {
  type EngineerId = String
  type ShipId = String
  type TurbineId = String

  implicit class DurationOps(duration: Duration) {
    def toFiniteDuration: FiniteDuration = {
      FiniteDuration(duration.toNanos(), TimeUnit.NANOSECONDS)
    }
    def toFiniteDuration(speedUp: Int): FiniteDuration = {
      FiniteDuration(duration.toNanos() / speedUp, TimeUnit.NANOSECONDS)
    }
  }
}
