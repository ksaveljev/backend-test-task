package com.intertrust.protocol

import java.time.Instant

sealed trait Alert

object Alert {
  final case class TurbineAlert(timestamp: Instant, turbineId: String, error: String) extends Alert
  final case class MovementAlert(timestamp: Instant, engineerId: String, error: String) extends Alert
}
