package com.intertrust.protocol

import java.time.Instant

import enumeratum.{Enum, EnumEntry}

import scala.collection.immutable.IndexedSeq

sealed trait TurbineStatus extends EnumEntry

object TurbineStatus extends Enum[TurbineStatus] {
  override val values: IndexedSeq[TurbineStatus] = findValues
  final case object Working extends TurbineStatus
  final case object Broken extends TurbineStatus
}

sealed trait Location {
  def id: String
}

object Location {
  final case class Vessel(id: String) extends Location
  final case class Turbine(id: String) extends Location
}


sealed trait Movement extends EnumEntry

object Movement extends Enum[Movement] {
  override val values: IndexedSeq[Movement] = findValues
  final case object Enter extends Movement
  final case object Exit extends Movement

}

sealed trait Event {
  def timestamp: Instant
}

object Event {
  final case class TurbineEvent(turbineId: String, status: TurbineStatus, generation: Double, timestamp: Instant) extends Event
  final case class MovementEvent(engineerId: String, location: Location, movement: Movement, timestamp: Instant) extends Event
}

