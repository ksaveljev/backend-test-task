package com.intertrust
package state

import com.intertrust.state.EngineerState._
import com.intertrust.state.EventConsumerCommand._
import com.intertrust.state.EventConsumerEvent._
import com.intertrust.state.TurbineState._
import com.intertrust.protocol._
import com.intertrust.protocol.Alert._
import com.intertrust.protocol.Location._
import com.intertrust.protocol.Movement._
import com.intertrust.protocol.TurbineStatus._
import com.typesafe.scalalogging.LazyLogging

import java.time.Instant
import scala.concurrent.duration._

/*
 * EventConsumer keeps using the structure below. All state updates happen here.
 * We keep information about
 * - lastUpdate: timestamp of event which we handled last
 * - engineers: all known engineers and their state
 * - turbines: all known turbines and their state
 */
final case class EventConsumerState(
  lastUpdate: Instant,
  engineers: Map[EngineerId, EngineerState],
  turbines: Map[TurbineId, TurbineState]
) extends LazyLogging {

  /*
   * For every state update we produce
   * - updated state
   * - a possible alert
   * - a list of commands to be scheduled (which might be empty)
   */
  type StateUpdate = (EventConsumerState, Option[Alert], List[ScheduleCommand])

  def update(event: EventConsumerEvent): StateUpdate = {
    event match {
      case e: TurbineStatusUpdate => onTurbineStatusUpdate(e)
      case e: EngineerUpdate      => onEngineerUpdate(e)
    }
  }

  def onTurbineStatusUpdate(event: TurbineStatusUpdate): StateUpdate = {
    turbines.get(event.turbineId) match {
      /*
       * Whenever we receive state update about turbine which we do not know anything about
       * and its status is Working we initialize its state only
       */
      case None if event.status == Working =>
        val turbineState = TurbineState(event.status, event.timestamp, NoEngineer(event.timestamp))
        (this.copy(lastUpdate = event.timestamp, turbines = this.turbines + (event.turbineId -> turbineState)), None, List.empty)

      /*
       * Whenever we receive state update about turbine which we do not know anything about
       * and its status is Broken we initialize its state the same way as for a Working
       * turbine but this time we need to also schedule a check in 4 hours to verify that
       * is has been fixed (or generate an alert otherwise)
       */
      case None if event.status == Broken =>
        val turbineState = TurbineState(event.status, event.timestamp, NoEngineer(event.timestamp))
        val alert = Some(TurbineAlert(event.timestamp, event.turbineId, "Turbine stopped working"))
        val scheduleCommand = List(ScheduleCommand(CheckIfStillBrokenAndNoEngineer(event.turbineId, event.timestamp), 4.hours))
        (this.copy(lastUpdate = event.timestamp, turbines = this.turbines + (event.turbineId -> turbineState)), alert, scheduleCommand)

      /*
       * Whenever we receive state update for a known turbine there are 4 cases which we
       * need to handle
       * - its status hasn't changed (Broken -> Broken and Working -> Working)
       * - its status has changed from Broken to Working
       * - its status has changed from Working to Broken
       */
      case Some(turbineState) =>
        (turbineState.turbineStatus, event.status) match {
          // for this case we only need to update the state of the turbine
          case (Broken, Working) =>
            val updatedState = turbineState.copy(turbineStatus = Working, since = event.timestamp)
            (this.copy(lastUpdate = event.timestamp, turbines = this.turbines + (event.turbineId -> updatedState)), None, List.empty)

          /*
           * for this case we need to
           * - update the state of the turbine
           * - generate an alert to notify that the turbine has stopped working
           * - schedule a command to check if any engineer has visited it in the last 4 hours
           */
          case (Working, Broken) =>
            val updatedState = turbineState.copy(turbineStatus = Broken, since = event.timestamp)
            val alert = Some(TurbineAlert(event.timestamp, event.turbineId, "Turbine stopped working"))
            val scheduleCommand = List(ScheduleCommand(CheckIfStillBrokenAndNoEngineer(event.turbineId, event.timestamp), 4.hours))
            (this.copy(lastUpdate = event.timestamp, turbines = this.turbines + (event.turbineId -> updatedState)), alert, scheduleCommand)

          // for this case the state of the turbine is untouched
          case (Broken, Broken) =>
            (this.copy(lastUpdate = event.timestamp), None, List.empty)

          // for this case the state of the turbine is untouched
          case (Working, Working) =>
            (this.copy(lastUpdate = event.timestamp), None, List.empty)
        }
    }
  }

  def onEngineerUpdate(event: EngineerUpdate): StateUpdate = {
    /*
     * Whenever an engineer enters a turbine we need to update the state of the turbine
     * to add this engineer to the list of engineers onboard.
     *
     * IMPROVE: due to the fact that we try to handle invalid cases (double entering,
     * double leaving, etc) as if there was some information (events) lost on the IoT
     * side then whenever an engineer enters the turbine we need to remove him from
     * other turbines (ideally he would be present on 0 or only 1 of them)
     */
    def enterTurbine(turbineId: TurbineId, engineerId: EngineerId, timestamp: Instant): TurbineState = {
      turbines.get(turbineId) match {
        /*
         * If we receive an event saying that an engineer enters a turbine and we know
         * nothing about that turbine then we add a default state for that turbine,
         * defaulting to Working status and adding this engineer as the only one onboard
         */
        case None =>
          TurbineState(Working, timestamp, HasEngineers(engineerId, List.empty, timestamp))

        // Whenever we know the state of the turbine we can easily add an engineer onboard
        case Some(turbineState) =>
          val updatedEngineerStatus = turbineState.engineerStatus match {
            case _: NoEngineer =>
              HasEngineers(engineerId, List.empty, timestamp)

            /*
             * Due to the fact that we try to handle invalid cases (double entering,
             * double leaving, etc) we would like to ensure not to duplicate engineer
             * being on the same turbine, and before adding a new one to the list of
             * engineers onboard we ensure that this engineer is not there already
             */
            case status: HasEngineers =>
              if (status.head == engineerId || status.tail.contains(engineerId)) {
                status
              } else {
                status.copy(tail = engineerId :: status.tail)
              }
          }
          turbineState.copy(engineerStatus = updatedEngineerStatus)
      }
    }

    /*
     * Whenever an engineer leaves a turbine we need to update the state of the turbine
     * to remove this engineer from the list of engineers onboard.
     *
     * IMPROVE: due to the fact that we try to handle invalid cases (double entering,
     * double leaving, etc) as if there was some information (events) lost on the IoT
     * side then whenever an engineer leaves the turbine we need to remove him from
     * other turbines (ideally he would be present on 0 or only 1 of them)
     */
    def leaveTurbine(turbineId: TurbineId, engineerId: EngineerId, timestamp: Instant): (TurbineState, Option[ScheduleCommand]) = {
      turbines.get(turbineId) match {
        /*
         * If we receive an event saying that an engineer leaves a turbine and we know
         * nothing about that turbine then we add a default state for that turbine,
         * defaulting to Working status and having no engineers onboard
         */
        case None =>
          (TurbineState(Working, timestamp, NoEngineer(timestamp)), None)

        /*
         * Due to the fact that our view of state might not be ideal (we might have started
         * receiving events after the fact that engineer entered the turbine, etc) we need
         * to carefully update the state of the turbine that this engineer is leaving.
         */
        case Some(turbineState) =>
          val updatedEngineerStatus = turbineState.engineerStatus match {
            /*
             * in case we do not know that the engineer entered this turbine it is currently
             * simply an ERROR message in the logs but depending on the actual specs it might
             * be a case where no such logging is needed
            */
            case status: NoEngineer =>
              logger.error(s"Leaving turbine [id=$turbineId] by engineer [id=$engineerId] when it has no engineers")
              status.copy(since = timestamp)
            // when this engineer is the only engineer onboard then we remove him
            case HasEngineers(eid, Nil, _) if eid == engineerId =>
              NoEngineer(timestamp)
            /*
             * when there is only one engineer onboard and it is not the engineer that is
             * leaving we are once again writing ERROR log but the spec might have something
             * different for this kind of case
             */
            case status @ HasEngineers(_, Nil, _) =>
              logger.error(s"Leaving turbine [id=$turbineId] by engineer[id=$engineerId] when he is not in the list of present engineers")
              status
            // removing an engineer when there are multiple engineers onboard
            case HasEngineers(eid, otherEid :: tail, since) if eid == engineerId =>
              HasEngineers(otherEid, tail, since)
            // removing an engineer when there are multiple engineers onboard
            case HasEngineers(eid, otherEngineers, since) if otherEngineers.contains(engineerId) =>
              HasEngineers(eid, otherEngineers.filterNot(_ == engineerId), since)
            // when there are multiple engineers onboard but not our engineer then we log an
            // ERROR but the spec might have something different for this kind of case
            case status: HasEngineers =>
              logger.error(s"Leaving turbine [id=$turbineId] by engineer[id=$engineerId] when he is not in the list of present engineers")
              status
          }
          /*
           * In case an engineer left a turbine which has a Broken status and there are no
           * more engineers left onboard we need to schedule a check in 3 minutes to verify
           * that the turbine has been fixed or send alert otherwise
           */
          val cmd = (turbineState.turbineStatus, updatedEngineerStatus) match {
            case (Broken, _: NoEngineer) => Some(ScheduleCommand(CheckIfStillBrokenAfterEngineer(turbineId, timestamp), 3.minutes))
            case _                       => None
          }
          (turbineState.copy(engineerStatus = updatedEngineerStatus), cmd)
      }
    }

    engineers.get(event.engineerId) match {
      /*
       * Whenever we do not have information about the engineer we need to consider 4 cases
       * - engineer enters ship
       * - engineer enters turbine
       * - engineer leaves ship
       * - engineer leaves turbine
       */
      case None =>
        (event.movement, event.location) match {
          // Entering the ship simply sets the state of this engineer to OnShip
          case (Enter, ship: Vessel) =>
            val engineerState = OnShip(ship.id, event.timestamp)
            (this.copy(lastUpdate = event.timestamp, engineers = this.engineers + (event.engineerId -> engineerState)), None, List.empty)

          // Entering the turbine sets engineer state to OnTurbine and updates the list of engineers on the turbine
          case (Enter, turbine: Turbine) =>
            val engineerState = OnTurbine(turbine.id, event.timestamp)
            val turbineState = enterTurbine(turbine.id, event.engineerId, event.timestamp)
            (this.copy(lastUpdate = event.timestamp, engineers = this.engineers + (event.engineerId -> engineerState), turbines = this.turbines + (turbine.id -> turbineState)), None, List.empty)

          // Leaving the ship sets the state of this engineer to Outside
          case (Exit, _: Vessel) =>
            val engineerState = Outside(event.timestamp)
            (this.copy(lastUpdate = event.timestamp, engineers = this.engineers + (event.engineerId -> engineerState)), None, List.empty)

          /*
           * Leaving the turbine updates the state of this engineer to Outside
           * It also updates the list of engineers on the turbine
           * And in case the turbine status is Broken and there are no more engineers onboard
           * there will be a scheduled command to check if the turbine is fixed in 3 minutes
           */
          case (Exit, turbine: Turbine) =>
            val engineerState = Outside(event.timestamp)
            val (turbineState, cmd) = leaveTurbine(turbine.id, event.engineerId, event.timestamp)
            (this.copy(lastUpdate = event.timestamp, engineers = this.engineers + (event.engineerId -> engineerState), turbines = this.turbines + (turbine.id -> turbineState)), None, cmd.toList)
        }

      /*
       * Whenever we have state of the engineer we need to consider all possible movements
       * including illogical ones (where we try to adapt as much as possible based on our
       * common sense but in the real project the spec would need to provide definite path
       * how to handle those cases - either dropping them entirely or saying what needs to
       * actually happen).
       */
      case Some(engineerState) =>
        (engineerState, event.movement, event.location) match {
          /*
           * Engineer is OnShip and he enters another ship. There are two possible scenarios:
           * - shipIds match, so it is a double enter event
           * - shipIds do not match, so we "lost" the exit event from the first ship
           */
          case (s: OnShip, Enter, ship: Vessel) =>
            // in both cases we generate an alert saying that we have a double enter event
            val msg = s"Engineer entered ship [id=${ship.id}] without leaving ship [id=${s.shipId}]"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            if (s.shipId == ship.id) {
              // in case shipIds match we do not touch the state of the engineer
              (this.copy(lastUpdate = event.timestamp), alert, List.empty)
            } else {
              // in case shipIds do not match we move the engineer from one ship to the other
              val updatedEngineers = this.engineers + (event.engineerId -> OnShip(ship.id, event.timestamp))
              (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers), alert, List.empty)
            }

          // Engineer is OnShip and he enters a turbine. So we "lost" the exit event from the ship
          case (s: OnShip, Enter, turbine: Turbine) =>
            val msg = s"Engineer entered turbine [id=${turbine.id}] without leaving ship [id=${s.shipId}]"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            val updatedEngineers = this.engineers + (event.engineerId -> OnTurbine(turbine.id, event.timestamp))
            val updatedTurbines = this.turbines + (turbine.id -> enterTurbine(turbine.id, event.engineerId, event.timestamp))
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), alert, List.empty)

          /*
           * Engineer is OnShip and he leaves a ship. There are two possible scenarios:
           * - shipIds match, so everything is good and logical
           * - shipIds do not match, so we "lost" the exit event from the first ship and enter event for the second ship
           *
           * We try to fix the state accordingly, exiting engineer from both ships
           */
          case (s: OnShip, Exit, ship: Vessel) =>
            val alert = if (s.shipId == ship.id) {
              None
            } else {
              val msg = s"Engineer left ship [id=${ship.id}] without leaving ship [id=${s.shipId}]"
              Some(MovementAlert(event.timestamp, event.engineerId, msg))
            }
            val updatedEngineers = this.engineers + (event.engineerId -> Outside(event.timestamp))
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers), alert, List.empty)

          /*
           * Engineer is OnShip and he leaves a turbine.
           * We assue that we "lost" the exit event from the ship and enter event for the turbine
           * And try to fix the state accordingly, leaving the ship, leaving the turbine, and
           * placing an engineer Outside.
           *
           * Leaving the turbine in a Broken status without any engineers onboard will 
           * generate a scheduled command to check if the turbine has been fixed in 3 minutes
           */
          case (s: OnShip, Exit, turbine: Turbine) =>
            val msg = s"Engineer left turbine [id=${turbine.id}] without leaving ship [id=${s.shipId}]"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            val (turbineState, cmd) = leaveTurbine(turbine.id, event.engineerId, event.timestamp)
            val updatedEngineers = this.engineers + (event.engineerId -> Outside(event.timestamp))
            val updatedTurbines = this.turbines + (turbine.id -> turbineState)
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), alert, cmd.toList)

          /*
           * Engineer is OnTurbine and he enters a ship.
           * We assume that we "lost" the exit event from the turbine
           * We try to fix the state accordingly, leaving the turbine and entering the ship
           *
           * Leaving the turbine in a Broken status without any engineers onboard will 
           * generate a scheduled command to check if the turbine has been fixed in 3 minutes
           */
          case (s: OnTurbine, Enter, ship: Vessel) =>
            val msg = s"Engineer entered ship [id=${ship.id}] without leaving turbine [id=${s.turbineId}]"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            val (turbineState, cmd) = leaveTurbine(s.turbineId, event.engineerId, event.timestamp)
            val updatedEngineers = this.engineers + (event.engineerId -> OnShip(ship.id, event.timestamp))
            val updatedTurbines = this.turbines + (s.turbineId -> turbineState)
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), alert, cmd.toList)

          /*
           * Engineer is OnTurbine and he leaves a turbine. There are two possible scenarios:
           * - turbineIds match, so everything is good and logical
           * - turbineIds do not match, so we "lost" the exit event from the first turbine and enter event for the second turbine
           *
           * In second case we try to fix the state accordingly leaving the first turbine and
           * entering the second. Leaving the turbine in a Broken status without any engineers onboard
           * will generate a scheduled command to check if the turbine has been fixed in 3 minutes
           */
          case (s: OnTurbine, Enter, turbine: Turbine) =>
            val msg = s"Engineer entered turbine [id=${turbine.id}] without leaving turbine [id=${s.turbineId}]"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            if (s.turbineId == turbine.id) {
              (this.copy(lastUpdate = event.timestamp), alert, List.empty)
            } else {
              val (turbineState, cmd) = leaveTurbine(s.turbineId, event.engineerId, event.timestamp)
              val enteredTurbineState = enterTurbine(turbine.id, event.engineerId, event.timestamp)
              val updatedEngineers = this.engineers + (event.engineerId -> OnTurbine(turbine.id, event.timestamp))
              val updatedTurbines = this.turbines + (s.turbineId -> turbineState) + (turbine.id -> enteredTurbineState)
              (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), alert, cmd.toList)
            }

          /*
           * Engineer is OnTurbine and he leaves a ship.
           * We assume that we "lost" the exit event from the turbine and enter event for the ship
           * We try to fix the state accordingly, leaving the turbine, entering the ship and leaving from it
           *
           * Leaving the turbine in a Broken status without any engineers onboard will 
           * generate a scheduled command to check if the turbine has been fixed in 3 minutes
           */
          case (s: OnTurbine, Exit, ship: Vessel) =>
            val msg = s"Engineer left ship [id=${ship.id}] without leaving turbine [id=${s.turbineId}]"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            val (turbineState, cmd) = leaveTurbine(s.turbineId, event.engineerId, event.timestamp)
            val updatedEngineers = this.engineers + (event.engineerId -> Outside(event.timestamp))
            val updatedTurbines = this.turbines + (s.turbineId -> turbineState)
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), alert, cmd.toList)

          /*
           * Engineer is OnTurbine and he leaves a turbine.
           * This is the case where turbineIds match, so the case is logical and correct
           *
           * Leaving the turbine in a Broken status without any engineers onboard will 
           * generate a scheduled command to check if the turbine has been fixed in 3 minutes
           */
          case (s: OnTurbine, Exit, turbine: Turbine) if s.turbineId == turbine.id =>
            val (turbineState, cmd) = leaveTurbine(turbine.id, event.engineerId, event.timestamp)
            val updatedEngineers = this.engineers + (event.engineerId -> Outside(event.timestamp))
            val updatedTurbines = this.turbines + (turbine.id -> turbineState)
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), None, cmd.toList)

          /*
           * Engineer is OnTurbine and he leaves a turbine.
           * This is the case where turbineIds do not match, so the case is illogical
           * We try to fix it by leaving the first turbine, entering the second one and leaving it
           *
           * Leaving the turbine in a Broken status without any engineers onboard will 
           * generate a scheduled command to check if the turbine has been fixed in 3 minutes
           *
           * As we leave two different turbines in this case we end up with a List of scheduled
           * commands which might be empty, have 1 or have 2 different commands to be scheduled
           */
          case (s: OnTurbine, Exit, turbine: Turbine) =>
            val msg = s"Engineer left turbine [id=${turbine.id}] without leaving turbine [id=${s.turbineId}]"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            val updatedEngineers = this.engineers + (event.engineerId -> Outside(event.timestamp))
            val (turbineState, cmd) = leaveTurbine(s.turbineId, event.engineerId, event.timestamp)
            val (otherTurbineState, otherCmd) = leaveTurbine(turbine.id, event.engineerId, event.timestamp)
            val updatedTurbines = this.turbines + (s.turbineId -> turbineState) + (turbine.id -> otherTurbineState)
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), alert, List(cmd, otherCmd).flatten)

          /*
           * Engineer is Outside and he leaves a ship.
           * We assume that we "lost" the enter event for the ship
           * We try to fix the state accordingly, entering the ship and leaving from it
           */
          case (_: Outside, Exit, ship: Vessel) =>
            val msg = s"Engineer left ship [id=${ship.id}] without entering"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            val updatedEngineers = this.engineers + (event.engineerId -> Outside(event.timestamp))
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers), alert, List.empty)

          /*
           * Engineer is Outside and he leaves a turbine.
           * We assume that we "lost" the enter event for the turbine
           * We try to fix the state accordingly, entering the turbine and leaving from it
           *
           * Leaving the turbine in a Broken status without any engineers onboard will 
           * generate a scheduled command to check if the turbine has been fixed in 3 minutes
           */
          case (_: Outside, Exit, turbine: Turbine) =>
            val msg = s"Engineer left turbine [id=${turbine.id}] without entering"
            val alert = Some(MovementAlert(event.timestamp, event.engineerId, msg))
            val (turbineState, cmd) = leaveTurbine(turbine.id, event.engineerId, event.timestamp)
            val updatedEngineers = this.engineers + (event.engineerId -> Outside(event.timestamp))
            val updatedTurbines = this.turbines + (turbine.id -> turbineState)
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), alert, cmd.toList)

          /*
           * Engineer is Outside and he enters a ship.
           * Logical and correct, nothing extra to handle
           */
          case (_: Outside, Enter, ship: Vessel) =>
            val updatedEngineers = this.engineers + (event.engineerId -> OnShip(ship.id, event.timestamp))
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers), None, List.empty)

          /*
           * Engineer is Outside and he enters a turbine.
           * Logical and correct, nothing extra to handle
           */
          case (_: Outside, Enter, turbine: Turbine) =>
            val updatedEngineers = this.engineers + (event.engineerId -> OnTurbine(turbine.id, event.timestamp))
            val updatedTurbines = this.turbines + (turbine.id -> enterTurbine(turbine.id, event.engineerId, event.timestamp))
            (this.copy(lastUpdate = event.timestamp, engineers = updatedEngineers, turbines = updatedTurbines), None, List.empty)
        }
    }
  }

  /*
   * Each time the scheduled command to verify that an engineer has entered the turbine
   * during the 4 hours after the turbine broke down is executed we verify the data here
   *
   * for specified turbineId and the timestamp it broke down we have several cases to check
   * (see below for the actual cases)
   */
  def hasEngineerEntered(turbineId: TurbineId, timestamp: Instant): Boolean = {
    turbines.get(turbineId) match {
      /*
       * if there is no information about the turbine it is something that should not really
       * happen therefore it currently logs an ERROR and the result is that no engineer
       * has visited the turbine since the timestamp
       */
      case None =>
        logger.error(s"Scheduled a hasEngineerEntered check but have no information about turbine [id=$turbineId]")
        false

      case Some(turbineState) =>
        turbineState.engineerStatus match {
          /*
           * when there are no engineers on the turbine at the time of the check then
           * there are two cases
           * - engineer did enter the turbine but left already
           * - engineer did not enter the turbine since the turbine broke down
           *
           * we simply check whether the 'since' timestamp (telling us when the last
           * engineer left the turbine) is after the 'timestamp' (which tells us when
           * the turbine actually broke down)
           */
          case NoEngineer(since) => since.isAfter(timestamp)
          // if there is at least one engineer on that turbine then there is no need for an alert
          // and the result is that the engineer did visit the turbine
          case _                 => true
        }
    }
  }

  /*
   * Each time the scheduled command to verify that the turbine has been fixed during 3 minutes
   * after an engineer left the turbine is executed we verify the data here
   *
   * for specified turbineId and the timestamp when an engineer left we have several cases to check
   * (see below for the actual cases)
   */
  def hasEngineerFixed(turbineId: TurbineId, timestamp: Instant): Boolean = {
    turbines.get(turbineId) match {
      /*
       * if there is no information about the turbine it is something that should not really
       * happen therefore it currently logs an ERROR and the result is that the turbine is
       * no longer in Broken status (we default to Working)
       */
      case None =>
        logger.error(s"Scheduled a hasEngineerFixed check but have no information about turbine [id=$turbineId]")
        false

      case Some(turbineState) =>
        turbineState.turbineStatus match {
          // if the status of the turbine is Working then it is obvious that an engineer has successfully fixed it
          case Working => true
          /*
           * when the status of the turbine is Broken then there are two cases we need to consider
           * - it was fixed by an engineer (after he left) but broke down again
           * - it wasn't fixed by an engineer (after he left) and it is still broken
           *
           * we simply check whether the turbine broke down (turbineState.since) after an engineer
           * left the turbine
           */
          case Broken  => turbineState.since.isAfter(timestamp)
        }
    }
  }
}
