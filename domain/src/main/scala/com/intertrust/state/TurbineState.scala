package com.intertrust
package state

import com.intertrust.protocol.TurbineStatus
import TurbineState._

import java.time.Instant

final case class TurbineState(turbineStatus: TurbineStatus, since: Instant, engineerStatus: EngineerStatus)

object TurbineState {
  sealed trait EngineerStatus
  final case class HasEngineers(head: EngineerId, tail: List[EngineerId], since: Instant) extends EngineerStatus
  final case class NoEngineer(since: Instant) extends EngineerStatus
}
