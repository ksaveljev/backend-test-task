package com.intertrust
package state

import java.time.Instant
import scala.concurrent.duration.FiniteDuration

sealed trait EventConsumerCommand

object EventConsumerCommand {
  final case class CheckIfStillBrokenAndNoEngineer(turbineId: TurbineId, since: Instant) extends EventConsumerCommand
  final case class CheckIfStillBrokenAfterEngineer(turbineId: TurbineId, since: Instant) extends EventConsumerCommand

  final case class ScheduleCommand(cmd: EventConsumerCommand, after: FiniteDuration)
}
