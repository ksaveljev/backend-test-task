package com.intertrust
package state

import com.intertrust.protocol._

import java.time.Instant

sealed trait EventConsumerEvent

object EventConsumerEvent {
  final case class EngineerUpdate(engineerId: EngineerId, location: Location, movement: Movement, timestamp: Instant) extends EventConsumerEvent
  final case class TurbineStatusUpdate(turbineId: TurbineId, status: TurbineStatus, timestamp: Instant) extends EventConsumerEvent
}
