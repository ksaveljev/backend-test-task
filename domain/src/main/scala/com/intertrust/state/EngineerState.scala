package com.intertrust
package state

import java.time.Instant

sealed trait EngineerState

/*
 * In order to track state of particular engineer (by his engineerId) we keep
 * track of his current position. For any particular engineer we might know that
 * - there is no information (haven't met any data about this engineer)
 * - he is Outside (neither on ship nor on turbine)
 * - he is OnShip (last known event was Enter Vessel)
 * - he is OnTurbine (last known event was Enter Turbine)
 */
object EngineerState {
  final case class Outside(since: Instant) extends EngineerState
  final case class OnShip(shipId: ShipId, since: Instant) extends EngineerState
  final case class OnTurbine(turbineId: TurbineId, since: Instant) extends EngineerState
}
