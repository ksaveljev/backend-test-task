package com.intertrust
package actors

import akka.actor.{ActorLogging, ActorRef, Props}
import akka.persistence._
import com.intertrust.state._
import com.intertrust.state.EventConsumerCommand._
import com.intertrust.state.EventConsumerEvent._
import com.intertrust.state.TurbineState._
import com.intertrust.protocol._
import com.intertrust.protocol.Alert._
import com.intertrust.protocol.Event._
import com.intertrust.protocol.TurbineStatus._

import java.time.Instant
import java.time.Duration
import scala.concurrent.duration._

class EventConsumer(speedUp: Int, alertsActor: ActorRef) extends PersistentActor with ActorLogging {
  import context.dispatcher

  var state: EventConsumerState = EventConsumerState(Instant.now, Map.empty, Map.empty)

  val persistenceId: String = "event-consumer"

  val receiveRecover: Receive = {
    case e: EventConsumerEvent                          => val _ = updateState(e)
    case SnapshotOffer(_, snapshot: EventConsumerState) => state = snapshot
    case RecoveryCompleted                              => scheduleAlertChecks()
  }

  val receiveCommand: Receive = {
    case e: MovementEvent                   => handleMovementEvent(e)
    case e: TurbineEvent                    => handleTurbineEvent(e)
    case c: CheckIfStillBrokenAndNoEngineer => checkBrokenNoEngineer(c)
    case c: CheckIfStillBrokenAfterEngineer => checkBrokenAfterEngineer(c)
  }

  def updateState(event: EventConsumerEvent): (Option[Alert], List[ScheduleCommand]) = {
    val (newState, alert, scheduleCommand) = state.update(event)
    state = newState
    (alert, scheduleCommand)
  }

  /*
   * This method is triggered right after the recover of this actor has been completed.
   * Before the actor crashed/restarted there might have been some commands scheduled
   * to check whether the turbine is fixed after an engineer left it or to check whether
   * an engineer visits a broken turbine. But with the restart of an actor those commands
   * are no longer scheduled.
   *
   * Right after actor recovery we go through the state of all known turbines and search
   * for the Broken turbines and restore the scheduled commands if the timestamps still
   * fit.
   */
  def scheduleAlertChecks(): Unit = {
    state.turbines.foreach {
      case (turbineId, turbineState) =>
        (turbineState.turbineStatus, turbineState.engineerStatus) match {
          case (Broken, NoEngineer(since)) =>
            val brokenDuration: FiniteDuration = Duration.between(turbineState.since, state.lastUpdate).toFiniteDuration
            val noEngineerDuration: FiniteDuration = Duration.between(since, state.lastUpdate).toFiniteDuration
            /*
             * In case when the turbine has been broken less than 4 hours and there hasn't
             * been a single engineer on it since it broke down we need to schedule a command
             * to check whether an engineer visits it during the first 4 hours after it broke
             */
            if (brokenDuration <= noEngineerDuration && brokenDuration < 4.hours) {
              val duration = (4.hours - brokenDuration) / speedUp.toLong
              val cmd = CheckIfStillBrokenAndNoEngineer(turbineId, turbineState.since)
              log.info(s"Scheduling $cmd in $duration")
              context.system.scheduler.scheduleOnce(duration, self, cmd)
            /*
             * In case when the turbine is in Broken state and the engineer left less than
             * 3 minutes ago we need to schedule a command to check whether an engineer
             * fixed the turbine or not
             */
            } else if (noEngineerDuration < brokenDuration && noEngineerDuration < 3.minutes) {
              val duration = (3.minutes - noEngineerDuration) / speedUp.toLong
              val cmd = CheckIfStillBrokenAfterEngineer(turbineId, since)
              log.info(s"Scheduling $cmd in $duration")
              context.system.scheduler.scheduleOnce(duration, self, cmd)
            }

          case _ =>
        }
    }
  }

  def handleMovementEvent(event: MovementEvent): Unit = {
    persist(EngineerUpdate(event.engineerId, event.location, event.movement, event.timestamp)) { e =>
      processEvent(e)
    }
  }

  def handleTurbineEvent(event: TurbineEvent): Unit = {
    persist(TurbineStatusUpdate(event.turbineId, event.status, event.timestamp)) { e =>
      processEvent(e)
    }
  }

  def processEvent(event: EventConsumerEvent): Unit = {
      val (alert, cmd) = updateState(event)
      alert.foreach { alertsActor ! _ }
      cmd.foreach { c =>
        log.info(s"Scheduling ${c.cmd} in ${c.after / speedUp.toLong}")
        context.system.scheduler.scheduleOnce(c.after / speedUp.toLong, self, c.cmd)
      }
  }

  def checkBrokenNoEngineer(cmd: CheckIfStillBrokenAndNoEngineer): Unit = {
    log.info(s"Running scheduled $cmd (state timestamp = ${state.lastUpdate})")
    if (!state.hasEngineerEntered(cmd.turbineId, cmd.since)) {
      val alert = TurbineAlert(Instant.now, cmd.turbineId, "Turbine has been broken for 4 hours and no Engineer entered")
      alertsActor ! alert
    }
  }

  def checkBrokenAfterEngineer(cmd: CheckIfStillBrokenAfterEngineer): Unit = {
    log.info(s"Running scheduled $cmd (state timestamp = ${state.lastUpdate}")
    if (state.hasEngineerFixed(cmd.turbineId, cmd.since)) {
      val alert = TurbineAlert(Instant.now, cmd.turbineId, "Turbine still broken after engineer left the turbine (3 minutes ago)")
      alertsActor ! alert
    }
  }
}

object EventConsumer {
  def props(speedUp: Int, alertsActor: ActorRef): Props = Props(new EventConsumer(speedUp, alertsActor))
}
