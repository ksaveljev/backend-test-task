package com.intertrust
package actors

import akka.actor.{ActorRef, ActorLogging, Props}
import akka.persistence.{PersistentActor, RecoveryCompleted, SnapshotOffer}
import com.intertrust.protocol.Event

import java.time.{Duration, Instant}
import scala.concurrent.duration.FiniteDuration

class EventEmitter[T <: Event](
  name: String,
  source: Iterator[T],
  simulationStartingPoint: Instant,
  speedUp: Int,
  eventConsumer: ActorRef
) extends PersistentActor
  with ActorLogging {

  import EventEmitter._
  import context.dispatcher

  var state: EventEmitterState = EventEmitterState(simulationStartingPoint, simulationStartingPoint, 0)

  val persistenceId: String = s"$name-emitter"

  val receiveRecover: Receive = {
    case e: EventSent                                  => updateState(e)
    case SnapshotOffer(_, snapshot: EventEmitterState) => state = snapshot
    case RecoveryCompleted                             => source.drop(state.numberOfEventsSent); scheduleNextEvent()
  }

  val receiveCommand: Receive = {
    case EmitEvent(event) =>
      persist(EventSent(event.timestamp)) { e =>
        updateState(e)
        log.info(s"Sending out $event")
        eventConsumer ! event
        scheduleNextEvent()
      }
  }

  def scheduleNextEvent(): Unit = {
    if (source.hasNext) {
      val event: T = source.next()
      val duration: FiniteDuration = Duration.between(state.simulationTime, event.timestamp).toFiniteDuration(speedUp)
      log.info(s"Scheduling next event ($event) in $duration")
      if (duration.toMillis <= 0) {
        self ! EmitEvent(event)
      } else {
        val _ = context.system.scheduler.scheduleOnce(duration, self, EmitEvent(event))
      }
    } else {
      log.info(s"No more events in source iterator. Stopping self with simulationTime = ${state.simulationTime}")
      context.stop(self)
    }
  }

  def updateState(event: EventSent): Unit = {
    state = state.copy(simulationTime = event.timestamp, numberOfEventsSent = state.numberOfEventsSent + 1)
  }
}

object EventEmitter {
  def props[T <: Event](
    name: String,
    source: Iterator[T],
    simulationStartingPoint: Instant,
    speedUp: Int,
    eventConsumer: ActorRef): Props = Props(new EventEmitter[T](name, source, simulationStartingPoint, speedUp, eventConsumer))

  final case class EventEmitterState(simulationStartingPoint: Instant, simulationTime: Instant, numberOfEventsSent: Int)

  sealed trait EmitterCommand
  final case class EmitEvent[T <: Event](event: T) extends EmitterCommand

  sealed trait EmitterEvent
  final case class EventSent(timestamp: Instant) extends EmitterEvent
}
