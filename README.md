#### Prerequisites

[Apache Cassandra](http://cassandra.apache.org/) is used as a storage for [Akka Persistence](https://doc.akka.io/docs/akka/current/persistence.html)

Currently it uses default configuration and expects Cassandra instance to be running on `127.0.0.1:9042`

#### Running

Running the sim is as simple as executing `sbt run`

Running tests is as simple as executing `sbt test`

#### Additional tools

- [scapegoat](https://github.com/sksamuel/sbt-scapegoat) for static code analysis
- [scoverage](https://github.com/scoverage/sbt-scoverage) for test coverage

You can run `sbt build` to run the full build process which includes these plugins.

You will be able to find scapegoat report under `target/scapegoat-report/index.html`

You will be able to find scoverage report under `target/scoverage-report/index.html`
