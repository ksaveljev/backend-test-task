import Dependencies._
import ScalacOptions._

lazy val backendTestTask = (project in file(".")
  dependsOn boot
  aggregate (domain, services, boot)
  enablePlugins(
    TestRootPlugin,
    ScapegoatRootPlugin,
    ScoverageRootPlugin,
  )
  settings(
    inThisBuild(List(
      organization     := "com.intertrust",
      description      := "Backend Test Task",
      scalaVersion     := "2.12.8",
      scalacOptions    ++= generalOptions,
      scapegoatVersion := "1.3.4",
      version          := "1.1"
    )),
    name := "backend-test-task",
    mainClass in Compile := Some("com.intertrust.Simulator"),
  )
)

lazy val domain = (project
  enablePlugins TestModulePlugin
  settings(
    libraryDependencies ++= Seq(
      externalDependencies.enumeratum,
      externalDependencies.logback,
      externalDependencies.scalaLogging,
      testDependencies.scalaTest
    )
  )
)

lazy val services = (project
  enablePlugins TestModulePlugin
  dependsOn (domain % "test->test;compile->compile")
  settings(
    libraryDependencies ++= Seq(
      externalDependencies.akkaActor,
      externalDependencies.akkaSlf4j,
      externalDependencies.akkaPersistence,
      testDependencies.scalaTest
    )
  )
)

lazy val boot = (project
  enablePlugins TestModulePlugin
  dependsOn services
  settings(
    libraryDependencies ++= Seq(
      externalDependencies.akkaActor,
      externalDependencies.akkaSlf4j,
      externalDependencies.akkaPersistence,
      externalDependencies.akkaPersistenceCassandra
    )
  )
)

addCommandAlias(
  "build",
  """|;
     |clean;
     |scapegoat; scapegoat:aggregateReports;
     |coverage; test; aggregateReports; coverageReport; coverageAggregate; scoveragePlugin:aggregateReports; coverageOff;
     |package;
  """.stripMargin)
